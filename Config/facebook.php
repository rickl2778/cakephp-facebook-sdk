<?php
  /**
   * Get an appId and secret from facebook and fill in this content.
   */
  $config = array(
  	'Facebook' => array(
  		'appId' => 'YOUR_APP_ID',
  		'secret' => 'YOUR_SECRET',
  		'cookie' => true
    )
  );
?>