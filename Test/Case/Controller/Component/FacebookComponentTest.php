<?php

App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Facebooksdk.Controller');
App::uses('FacebookComponent', 'Facebooksdk.Controller/Component');

// A fake controller to test against
class TestFacebookController extends Controller {
}

class FacebookComponentTest extends CakeTestCase {

  public $Facebook = null;
  public $Controller = null;

  public function setUp() {
    parent::setUp();
    // Setup our component and fake test controller
    $Collection = new ComponentCollection();
    $this->Facebook = new FacebookComponent($Collection);
    $this->Controller = new TestFacebookController();
    $this->Facebook->initialize($this->Controller);
  }
  
  public function tearDown() {
    parent::tearDown();
    // Clean up after we're done
    unset($this->Facebook);
    unset($this->Controller);
  }  

  /**
   * test Graph API
   */
  public function testApi() {
    // if wrong data in Config/facebook.php will throw a FacebookApiException
    $result = $this->Facebook->Sdk->api(19292868552, 'GET');
    $expected = 'Facebook Platform';
    $this->assertEquals($result['name'], $expected);
  }
}