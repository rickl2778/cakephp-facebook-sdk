# Facebook SDK Plugin for CakePHP #

Version 1.0

The Facebook SDK plugin for CakePHP provides a (very) thin veil for CakePHP 2.x applications over the [official Facebook PHP SDK](http://github.com/facebook/facebook-php-sdk).

## Usage ##

To use the Facebook SDK plugin for requests you must populate the following two lines in your `/app/Plugin/Twitteroauth/Config/facebook.php` file.

    'appId' => 'YOUR_APP_ID',
    'secret' => 'YOUR_SECRET',

Don't forget to replace the placeholder text with your actual keys!

An appId and secret can be obtained for free from the [developer Facebook website](http://developers.facebook.com/).

Controllers that will be using the Facebook PHP SDK require the FacebookComponent to be included.

    public $components = array('Facebooksdk.Facebook');

In the controller simply call the Facebook PHP SDK method now available in the $this->Facebook->Sdk class.

    $result = $this->Facebook->Sdk->api(19292868552, 'GET');

For Facebook PHP SDK documentation and methods, check http://developers.facebook.com/docs/reference/php/

## Requirements ##

* PHP version: PHP 5.2+
* CakePHP version: Cakephp 2.0+